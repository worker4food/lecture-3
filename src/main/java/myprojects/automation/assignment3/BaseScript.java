package myprojects.automation.assignment3;

import myprojects.automation.assignment3.utils.Logger;
import myprojects.automation.assignment3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    /**
     *
     * @return New instance of {@link WebDriver} object. Driver type is based on passed parameters
     * to the automation project, returns {@link ChromeDriver} instance by default.
     */
    public static WebDriver getDriver() {
        String prefix  = "/" + System.getProperty("os.name").split("\\s")[0];
        String suffix  = prefix.startsWith("/Windows") ? ".exe" : "";
        BiConsumer<String, String> wrap = (varName, fileName) -> {
             System.setProperty(
                    varName,
                    new File(BaseScript.class.getResource(prefix + fileName + suffix).getFile()).getPath());
        };

        String browser = Properties.getBrowser();

        switch (browser) {
            case "firefox":
                wrap.accept("webdriver.gecko.driver", "/geckodriver");
                return new FirefoxDriver();
            case "ie":
            case "internet explorer":
                wrap.accept("webdriver.ie.driver", "/IEDriverServer");
                return new InternetExplorerDriver();
            case "chrome":
            default:
                wrap.accept("webdriver.chrome.driver", "/chromedriver");
                return new ChromeDriver();
        }
    }

    /**
     * Creates {@link WebDriver} instance with timeout and browser window configurations.
     *
     * @return New instance of {@link EventFiringWebDriver} object. Driver type is based on passed parameters
     * to the automation project, returns {@link ChromeDriver} instance by default.
     */
    public static EventFiringWebDriver getConfiguredDriver() {
        EventFiringWebDriver evtWD = new EventFiringWebDriver(getDriver());

        evtWD.manage().window().maximize();
        evtWD.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        evtWD.register(new Logger());

        return evtWD;
    }
}
